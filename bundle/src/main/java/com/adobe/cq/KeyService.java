package com.adobe.cq;

/**
 * Created by jimmy on 2017-05-17.
 */
public interface KeyService {
    public void setKey(int val);
    public String getKey();
    public String getMessage();
}
