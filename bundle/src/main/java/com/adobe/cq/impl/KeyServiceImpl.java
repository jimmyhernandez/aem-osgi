package com.adobe.cq.impl;

import com.adobe.cq.KeyService;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;

@Component
@Service
public class KeyServiceImpl implements KeyService {

    private int key = 0;


    public void setKey(int val) {
        this.key = val;
    }

    public String getKey() {
        return Integer.toString(this.key);
    }

    public String getMessage() {
        return "Component Service OSGi";
    }
}
